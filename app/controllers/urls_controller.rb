class UrlsController < ApplicationController
  
  # GET /urls
  # GET /urls.json
  def index
    @urls = Url.all
  end

  
  # GET /urls/new
  def new
    @url = Url.new
  end

  def show
    @url = Url.find_by(code: params[:code])
    
    respond_to do |format|
      if @url.blank?
        format.html { redirect_to list_path, notice: "Url not found" }
        format.json { render json: ["Url not found"], status: :unprocessable_entity }
      else
        format.html { @url.update_attributes(usage: @url.usage+1); redirect_to @url.url}
        format.json { render :show }
      end
    end
  end

  # POST /urls
  # POST /urls.json
  def create
    @url = Url.new(url_params)
    respond_to do |format|
      if @url.save
        format.html { redirect_to list_path, notice: 'Url was successfully created.' }
        format.json { render :show, status: :created, location: @url }
      else
        format.html { render :new }
        format.json { render json: @url.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /urls/1
  # DELETE /urls/1.json
  def destroy
    url = Url.find(params[:id])
    respond_to do |format|
      format.html { redirect_to list_path, notice: url.destroy ? 'Url was successfully destroyed.' : 'Some error occured.' }
      format.json { head :no_content }
    end
  end

  private
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def url_params
      params.require(:url).permit(:url,:code)
    end
end
