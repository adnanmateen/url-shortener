class Url < ActiveRecord::Base
	
	before_validation :create_code
	validates :url , presence: true, uniqueness: true, url: true
	validates :code, uniqueness: {message: "already in use"}
	validate :validate_url


	def self.destroy_old_urls
		Url.where('created_at <= ?', 15.days.ago).destroy_all
	end

	def short_url
		"#{ENV["URL"]}#{code}"
	end

	private
	def create_code
		self.code = SecureRandom.hex(4) if code.blank?
	end

	def validate_url
		error = false
		begin
		    Timeout::timeout(5) {
			    uri = URI.parse(url)
			    http = Net::HTTP.new(uri.host, uri.port)
			    http.use_ssl = uri.port.to_i != 80
			    request = Net::HTTP::Get.new(uri.request_uri)
			    res = http.request(request)
			    if res.code.to_i > 400
			    	error = true
			    end

			}
	   	rescue => e
	   		puts e
	   		error = true
	   	end
	   	if error
	   		errors.add(:base, "Url is not working")
	   	end
	end
end
