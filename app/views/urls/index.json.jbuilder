json.array!(@urls) do |url|
  json.extract! url, :code, :url, :short_url
end
