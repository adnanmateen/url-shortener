class CreateUrls < ActiveRecord::Migration
  def change
    create_table :urls do |t|
      t.string :code
      t.string :url
      t.integer :usage, default: 0
      t.timestamps null: false
    end
  end
end
